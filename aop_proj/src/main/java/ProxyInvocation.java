import interceptor.intercep.MethodInterceptor;
import Invocation.MethodInvocation;
import java.lang.reflect.Method;
import java.util.List;

public class ProxyInvocation implements MethodInvocation {

    private final Method method;
    private Object[] objects;
    private final Object target;
    private final List<?> interceptors;

    private int counter = 0;

    public ProxyInvocation(Method method, Object[] objects, Object target, List<?> interceptors) {
        this.method = method;
        this.objects = objects;
        this.target = target;
        this.interceptors = interceptors;
    }

    @Override
    public Method getMethod() {
        return null;
    }

    @Override
    public Object[] getArguments() {
        return new Object[0];
    }

    @Override
    public Object getTarget() {
        return null;
    }

    @Override
    public Object proceed() throws Throwable {
        if (counter >= interceptors.size()) {
            return method.invoke(target, objects);
        }
        MethodInterceptor nextInterceptor = (MethodInterceptor) interceptors.get(counter);

        counter++;
        return nextInterceptor.invoke(this);
    }
}
