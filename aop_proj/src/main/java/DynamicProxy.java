import annotations.*;
import interceptor.advice.*;
import interceptor.intercep.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class DynamicProxy implements InvocationHandler {

    private Object aspect;
    private Object target;


    public DynamicProxy(Object aspect, Object target) {
        this.aspect = aspect;
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        List<MethodInterceptor> interceptorList = getInterceptor();
        ProxyInvocation proxyInvocation = new ProxyInvocation(method, args, target, interceptorList);
        return proxyInvocation.proceed();
    }

    private List<MethodInterceptor> getInterceptor() {
        if(aspect == null) {
            return new ArrayList<>();
        }
        Class<?> cla = aspect.getClass();
        Method[] methods = cla.getDeclaredMethods();
        List<MethodInterceptor> list = new ArrayList<>();
        for(Method method: methods) {
            Annotation[] annotations = method.getDeclaredAnnotations();
            for(Annotation an: annotations) {
                if(an.annotationType() == Before.class) {
                    BeforeAdvice before = new AspectBeforeAdvice(method, aspect);
                    MethodInterceptor methodInterceptor = new BeforeAdviceInterceptor(before);
                    list.add(methodInterceptor);
                } else if(an.annotationType() == After.class) {
                    AfterAdvice after = new AspectAfterAdvice(method, aspect);
                    MethodInterceptor methodInterceptor = new AfterAdviceInterceptor(after);
                    list.add(methodInterceptor);
                } else if(an.annotationType() == Around.class) {
                    AroundAdviceInterceptor around = new AroundAdviceInterceptor(method, aspect);
                    list.add(around);
                }else if (an.annotationType() == AfterReturn.class) {
                    AspectAfterReturnAdvice afterReturn = new AspectAfterReturnAdvice(method, aspect);
                    MethodInterceptor methodInterceptor = new AfterReturnInterceptor(afterReturn);
                    list.add(methodInterceptor);
                } else if (an.annotationType() == AfterThrow.class) {
                    AfterThrowAdvice afterThrow = new AspectAfterThrowAdvice(method, aspect);
                    MethodInterceptor methodInterceptor = new AfterThrowInterceptor(afterThrow);
                    list.add(methodInterceptor);
                }
            }
        }

        return list;
   }
}

