package Invocation;

public interface Invocation extends Joinpoint {
    Object[] getArguments();
}
