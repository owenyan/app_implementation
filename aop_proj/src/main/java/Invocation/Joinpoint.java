package Invocation;

import java.lang.reflect.Method;

public interface Joinpoint {

    Object proceed() throws Throwable;

    Method getMethod();

    Object getTarget();
}
