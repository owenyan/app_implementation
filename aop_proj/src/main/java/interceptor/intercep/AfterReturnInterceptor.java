package interceptor.intercep;

import Invocation.MethodInvocation;
import interceptor.advice.Advice;
import interceptor.advice.AfterReturnAdvice;

public class AfterReturnInterceptor implements MethodInterceptor, Advice {
    private AfterReturnAdvice afterReturnAdvice;

    public AfterReturnInterceptor(AfterReturnAdvice afterReturnAdvice) {
        this.afterReturnAdvice = afterReturnAdvice;
    }


    @Override
    public Object invoke(MethodInvocation methodInterceptor) throws Throwable {
        Object returnValue = methodInterceptor.proceed();
        afterReturnAdvice.execute(methodInterceptor.getMethod(), methodInterceptor.getArguments(), methodInterceptor.getTarget());
        return returnValue;


//        AfterReturnInterceptor
//                    public Object invoke(MethodInvocation mi) {
//                          Object retVal = mi.proceed();
//                           afterReturnAdvice(retVal)
//                           return result
//                        }
    }
}
