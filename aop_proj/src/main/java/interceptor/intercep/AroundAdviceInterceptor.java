package interceptor.intercep;

import Invocation.MethodInvocation;
import interceptor.advice.Advice;

import java.lang.reflect.Method;

public class AroundAdviceInterceptor implements MethodInterceptor, Advice {

    private Object instance;
    private Method method;

    public AroundAdviceInterceptor(Method method, Object instance) {
        this.instance = instance;
        this.method = method;
    }

    @Override
    public Object invoke(MethodInvocation methodInterceptor) throws Throwable {
        return method.invoke(instance, methodInterceptor);
    }
}
