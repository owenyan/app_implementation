package interceptor.intercep;

import Invocation.MethodInvocation;
import interceptor.advice.Advice;
import interceptor.advice.AfterThrowAdvice;

public class AfterThrowInterceptor implements MethodInterceptor, Advice {
    private AfterThrowAdvice afterThrowAdvice;

    public AfterThrowInterceptor(AfterThrowAdvice afterThrowAdvice) {
        this.afterThrowAdvice = afterThrowAdvice;
    }

    @Override
    public Object invoke(MethodInvocation methodInterceptor) throws Throwable {
        Object returnValue = methodInterceptor.proceed();
        //afterThrowAdvice.execute(methodInterceptor.getMethod(), methodInterceptor.getArguments(), methodInterceptor.getTarget());
        return returnValue;
    }
}
