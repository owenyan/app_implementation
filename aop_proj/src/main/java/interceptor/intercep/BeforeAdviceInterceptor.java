package interceptor.intercep;

import Invocation.MethodInvocation;
import interceptor.advice.Advice;
import interceptor.advice.BeforeAdvice;

public class BeforeAdviceInterceptor implements MethodInterceptor, Advice {

    private BeforeAdvice beforeAdvice;
    public BeforeAdviceInterceptor(BeforeAdvice beforeAdvice) {
        this.beforeAdvice = beforeAdvice;
    }

    @Override
    public Object invoke(MethodInvocation methodInterceptor) throws Throwable {
        beforeAdvice.execute(methodInterceptor.getMethod(), methodInterceptor.getArguments(), methodInterceptor.getTarget());
        return methodInterceptor.proceed();


//        BeforeInterceptor
//                *     public Object invoke(MethodInvocation mi) {  5.
//                *         beforeAdvice();  6.
//                *         return mi.proceed();  7.
//                *     }
    }
}
