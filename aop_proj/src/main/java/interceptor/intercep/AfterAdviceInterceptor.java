package interceptor.intercep;

import Invocation.MethodInvocation;
import interceptor.advice.Advice;
import interceptor.advice.AfterAdvice;

public class AfterAdviceInterceptor implements MethodInterceptor, Advice {
    private AfterAdvice afterAdvice;

    public AfterAdviceInterceptor(AfterAdvice afterAdvice) {
        this.afterAdvice = afterAdvice;
    }

    @Override
    public Object invoke(MethodInvocation methodInterceptor) throws Throwable {
        Object returnValue = methodInterceptor.proceed();
        afterAdvice.execute(methodInterceptor.getMethod(), methodInterceptor.getArguments(), methodInterceptor.getTarget());
        return returnValue;

//        AfterInterceptor
//                  public Object invoke(MethodInvocation mi) { 11.
//                      Object retVal = mi.proceed();
//                        afterAdvice()
//                        return retVal;
    }
}
