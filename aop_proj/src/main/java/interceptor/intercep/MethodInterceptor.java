package interceptor.intercep;

import Invocation.MethodInvocation;
import interceptor.advice.Advice;

public interface MethodInterceptor extends Advice {
    Object invoke(MethodInvocation methodInterceptor) throws Throwable;
}
