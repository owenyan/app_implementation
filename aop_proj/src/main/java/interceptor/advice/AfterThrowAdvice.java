package interceptor.advice;

import interceptor.advice.Advice;

import java.lang.reflect.Method;

public interface AfterThrowAdvice extends Advice {
    Object execute(Method method, Object[] args, Object target) throws Throwable;
}
