package interceptor.advice;

import interceptor.advice.AfterThrowAdvice;

import java.lang.reflect.Method;

public class AspectAfterThrowAdvice implements AfterThrowAdvice {

    private Method method;
    private Object aspectObj;

    public AspectAfterThrowAdvice(Method method, Object aspectObj) {
        this.method = method;
        this.aspectObj = aspectObj;
    }

    @Override
    public Object execute(Method method, Object[] args, Object target) throws Throwable {
        this.method.invoke(aspectObj);
        return null;
    }
}
