package interceptor.advice;

import java.lang.reflect.Method;

public class AspectBeforeAdvice implements BeforeAdvice {

    private Method aspectMethod;
    private Object aspectObj;

    public AspectBeforeAdvice(Method method, Object aspectObj) {
        this.aspectMethod = method;
        this.aspectObj = aspectObj;
    }
    @Override
    public Object execute(Method method, Object[] args, Object target) throws Throwable {
        this.aspectMethod.invoke(aspectObj);
        return null;
    }
}
