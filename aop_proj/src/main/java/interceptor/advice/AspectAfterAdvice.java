package interceptor.advice;

import java.lang.reflect.Method;

public class AspectAfterAdvice implements AfterAdvice {
    private Method aspectMethod;
    private Object aspectObj;

    public AspectAfterAdvice(Method aspectMethod, Object aspectObj) {
        this.aspectMethod = aspectMethod;
        this.aspectObj = aspectObj;
    }

    @Override
    public Object execute(Method method, Object[] args, Object target) throws Throwable {
        this.aspectMethod.invoke(aspectObj);
        return null;
    }
}
