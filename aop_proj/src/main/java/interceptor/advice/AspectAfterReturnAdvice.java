package interceptor.advice;

import interceptor.advice.AfterReturnAdvice;

import java.lang.reflect.Method;

public class AspectAfterReturnAdvice implements AfterReturnAdvice {

    private Method method;
    private Object aspectObj;

    public AspectAfterReturnAdvice(Method method, Object aspectObj) {
        this.method = method;
        this.aspectObj = aspectObj;
    }
    @Override
    public Object execute(Method method, Object[] args, Object target) throws Throwable {
        this.method.invoke(aspectObj);
        return null;
    }
}
