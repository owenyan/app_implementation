import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

class Test {
    public static void main(String[] args) {
        TeacherAspect teacherAspect = new TeacherAspect();
        TeacherService teacherService = new TeacherServiceImplementation();
        InvocationHandler invocationHandler = new DynamicProxy(teacherAspect, teacherService);
        TeacherService proxy = (TeacherService) Proxy.newProxyInstance(Test.class.getClassLoader(), new Class[]{TeacherService.class}, invocationHandler);
        proxy.print();
    }
}
