import Invocation.MethodInvocation;
import annotations.*;


public class TeacherAspect {
    @Before
    public void m1() throws Throwable {
        System.out.println("before");
    }
    
    @After
    public void m2() throws Throwable {
        System.out.println("after");
    }

    @AfterReturn
    public void m4() throws Throwable {
        System.out.println("AfterReturn");
    }

    @AfterThrow
    public void m5() throws Throwable {
        System.out.println("afterThrow");
    }

    @Around
    public void m3(MethodInvocation methodInvocation) throws Throwable {
        System.out.println("around before logic");
        methodInvocation.proceed();
        System.out.println("around after logic");
    }

}
